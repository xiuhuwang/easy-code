import helpers from '@/utils/helpers/base'

export default helpers.keyMirror({
  PAGE_DICTIONARYS: null,
  LIST_DICTIONARY_TYPES: null,
  ADD_DICTIONARY: null,
  MODIFY_DICTIONARY: null,
  SHOW_DICTIONARY: null,
  REMOVE_DICTIONARY: null,
  LOADING: null
})
