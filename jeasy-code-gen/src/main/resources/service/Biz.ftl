package ${conf.basePackage}.${table.lowerCamelName}.biz;

import ${conf.basePackage}.common.spring.SpringContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * ${table.comment} Biz
 *
 * @author ${conf.author}
 * @version ${conf.version}
 * @since ${conf.createDate}
 */
@Slf4j
@Component
public class ${table.className}Biz {

    public static ${table.className}Biz me() {
        return SpringContextHolder.getBean(${table.className}Biz.class);
    }
}
