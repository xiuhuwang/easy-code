package com.jeasy.log.dao;

import com.jeasy.base.mybatis.dao.BaseDAO;
import com.jeasy.log.entity.LogEntity;

/**
 * 日志 DAO
 *
 * @author taomk
 * @version 1.0
 * @since 2017/11/08 16:33
 */
public interface LogDAO extends BaseDAO<LogEntity> {
}
