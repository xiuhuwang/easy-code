import types from './types'

export default {
  [types.PAGE_DICTIONARYS] (state, payload) {
    state.items = payload.data.list
    state.total = payload.data.totalCount
    state.loading = false
  },
  [types.SHOW_DICTIONARY] (state, payload) {
    state.item = payload.data.entity
  },
  [types.LIST_DICTIONARY_TYPES] (state, payload) {
    state.dictionaryTypes = payload.data.list
  },
  [types.LOADING] (state, payload) {
    state.loading = true
  }
}
