'use strict'
// Log神器chalk
const chalk = require('chalk')
// 语义化版本（Semantic Versioning）规范 的一个实现, 实现了版本和版本范围的解析、计算、比较
const semver = require('semver')
const packageConfig = require('../package.json')
// shelljs模块重新包装了child_process,调用系统命令更加简单。
// shelljs是Unix Shell在Node.js API层的轻量级实现，可以支持Windows、Linux、OS X。你可以像在Unix命令行敲命令一样书写代码
const shell = require('shelljs')

function exec (cmd) {
  // Node.js进程通信模块child_process, 通过它可以实现创建多进程，以利用单机的多核计算资源。
  return require('child_process').execSync(cmd).toString().trim()
}

const versionRequirements = [
  {
    name: 'node',
    // process 对象是一个 global （全局变量），提供有关信息，控制当前 Node.js 进程。
    // 作为一个对象，它对于 Node.js 应用程序始终是可用的，故无需使用 require()。
    currentVersion: semver.clean(process.version),
    versionRequirement: packageConfig.engines.node
  }
]

if (shell.which('npm')) {
  versionRequirements.push({
    name: 'npm',
    currentVersion: exec('npm --version'),
    versionRequirement: packageConfig.engines.npm
  })
}

module.exports = function () {
  const warnings = []
  for (let i = 0; i < versionRequirements.length; i++) {
    const mod = versionRequirements[i]
    if (!semver.satisfies(mod.currentVersion, mod.versionRequirement)) {
      warnings.push(mod.name + ': ' +
        chalk.red(mod.currentVersion) + ' should be ' +
        chalk.green(mod.versionRequirement)
      )
    }
  }

  if (warnings.length) {
    console.log('')
    console.log(chalk.yellow('To use this template, you must update following to modules:'))
    console.log()
    for (let i = 0; i < warnings.length; i++) {
      const warning = warnings[i]
      console.log('  ' + warning)
    }
    console.log()
    process.exit(1)
  }
}
